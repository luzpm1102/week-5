import React, { useEffect, useState } from 'react';
import { routes } from '../App';
import logo from '../assets/img/game.png';
import { useLocalStorage } from '../hooks/useLocalStorage';
import '../styles/navbar.scss';

export const NavBar = ({ setNavigation }) => {
  const { getItem } = useLocalStorage();
  const [token, setToken] = useState(getItem('token'));

  useEffect(() => {
    setToken(getItem('token'));
  }, [token]);

  return (
    <header>
      <div className='navbar'>
        <img className='navbar__logo' src={logo} alt='logo' />
        <div
          className='navbar__title'
          onClick={() => setNavigation(routes.Home)}
        >
          <h2>Game Store</h2>
        </div>
        <div
          onClick={() => setNavigation(routes.GameList)}
          className='navbar__title'
        >
          <h3 className='navbar__title--position'>Game List</h3>
        </div>
        <div
          onClick={() => setNavigation(routes.Login)}
          className='navbar__title'
        >
          {!token ? (
            <h3 className='navbar__title--position'>Login</h3>
          ) : (
            <h3 className='navbar__title--position'>Logout</h3>
          )}
        </div>
      </div>
    </header>
  );
};
