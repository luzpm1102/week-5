import React from 'react';
import { defaultImage } from '../helpers/constants';
import '../styles/games.scss';

const Game = ({ image, name, price, id, setNavigation }) => {
  return (
    <a onClick={() => setNavigation(id)}>
      <div className='card-container'>
        <div className='card-container__header'>
          <img src={image ? image : defaultImage} alt='' />
        </div>
        <div className='card-container__body'>
          <h4>{name}</h4>
        </div>
        <div className='card-container__price'>
          <div className='card-container__price__price-text'>{'$' + price}</div>
        </div>
      </div>
    </a>
  );
};

export default Game;
