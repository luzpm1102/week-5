import React from 'react';
import '../styles/games.scss';

export const Comment = ({ comment, username }) => {
  return (
    <div className='comment-container'>
      <div className='comment__username'>
        <h5>{username}</h5>
      </div>
      <hr />
      <div className='comment__body'>
        <p>{comment}</p>
      </div>
    </div>
  );
};
