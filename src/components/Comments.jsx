import React, { useMemo, useState } from 'react';
import { GAMES } from '../helpers/constants';
import { checkInput, handleChange, setInitialState } from '../helpers/helpers';
import { useApi } from '../hooks/useApi';
import { useLocalStorage } from '../hooks/useLocalStorage';
import { usePagination } from '../hooks/usePagination';
import { post } from '../services/post';
import { Comment } from './Comment';
import Pagination from './Pagination';

export const Comments = ({ id }) => {
  const uri = `${GAMES}${id}/comments?_limit=-1`;
  const url = `${GAMES}${id}/comment`;
  const { data, loading } = useApi(uri);
  const { getItem } = useLocalStorage();
  const [token, setToken] = useState(getItem('token'));

  const [body, setBody] = useState('');

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(6);

  const comments = useMemo(
    () => displayComments(data, indexOfTheFirstItem, indexOfTheLastItem),
    [indexOfTheFirstItem, indexOfTheLastItem, data]
  );

  const sendComment = async () => {
    if (!checkInput('Comment', body)) {
      return;
    }
    const comment = {
      body: body,
    };
    try {
      await post(url, comment, token);
      alert('Commented');
      setInitialState(setBody);
      setToken(getItem('token'));
    } catch (error) {
      alert(error);
    }
  };

  return (
    <>
      {loading.current && (
        <div className='loading-container'>
          <div className='loading'></div>
        </div>
      )}
      {!loading.current && (
        <div className='game-comments__comment'>
          {comments}
          <Pagination
            itemsPerPage={itemsPerPage}
            totalpages={
              data.filter((comment) => comment.body.trim() !== '').length
            }
            paginate={paginate}
            currentPage={currentPage}
          />
        </div>
      )}
      {!token ? (
        <div className='game-comments__title'>
          <h3>Log in to comment</h3>
        </div>
      ) : (
        <div className='game-comments__user-comment'>
          <input
            type='text'
            value={body}
            onChange={(e) => handleChange(e, setBody)}
            className='game-comments__input'
          />

          <button
            className='game-comments__button button-primary'
            onClick={sendComment}
          >
            Send
          </button>
        </div>
      )}
    </>
  );
};

const displayComments = (data, indexOfTheFirstItem, indexOfTheLastItem) => {
  const result = data
    .sort((a, b) => {
      var dateA = new Date(a.created_at);
      var dateB = new Date(b.created_at);
      return dateB - dateA;
    })
    .filter((comment) => comment.body.trim() != '');

  const resultFiltered = result
    .slice(indexOfTheFirstItem, indexOfTheLastItem)
    .map((comment) => (
      <Comment
        comment={comment.body}
        username={comment.user.username}
        key={comment.id}
      />
    ));

  return resultFiltered;
};
