import React from 'react';
import { useApi } from '../hooks/useApi';
import Game from './Game';
import Pagination from './Pagination';
import '../styles/games.scss';
import { GAMES } from '../helpers/constants';
import { usePagination } from '../hooks/usePagination';

export const Games = ({ setNavigation }) => {
  const { data, loading } = useApi(GAMES);

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(6);

  const displayGames = data
    .slice(indexOfTheFirstItem, indexOfTheLastItem)
    .map(({ name, price, id, cover_art }) => (
      <Game
        key={id}
        image={cover_art ? cover_art.formats.small.url : cover_art}
        name={name}
        price={price}
        id={id}
        setNavigation={setNavigation}
      />
    ));

  return (
    <div>
      {data.length > 0 && !loading.current && (
        <div>
          <div className='cards-container'>{displayGames}</div>
          <Pagination
            itemsPerPage={itemsPerPage}
            totalpages={data.length}
            paginate={paginate}
            currentPage={currentPage}
          />
        </div>
      )}
      {loading.current && (
        <div className='loading-container'>
          <div className='loading'></div>
        </div>
      )}
    </div>
  );
};
