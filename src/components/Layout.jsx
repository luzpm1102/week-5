import React from 'react';
import { MemoizedFooter } from './Footer';
import { NavBar } from './NavBar';

export const Layout = ({ children, setNavigation }) => {
  return (
    <>
      <div className='home-container'>
        <NavBar setNavigation={setNavigation} />
        <div className='main-container'>{children}</div>

        <MemoizedFooter />
      </div>
    </>
  );
};
