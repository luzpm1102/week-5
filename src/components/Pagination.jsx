import React, { useState } from 'react';
import '../styles/pagination.scss';

const Pagination = ({ itemsPerPage, totalpages, paginate, currentPage }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalpages / itemsPerPage); i++) {
    pageNumbers.push(i);
  }

  const [isActive, setIsActive] = useState(false);

  const addClass = () => {
    setIsActive(!isActive);
  };

  return (
    <nav className='pagination-container'>
      <ul className='pagination-list'>
        <li className='pagination-list__item '>
          <a
            className='pagination-list__link'
            onClick={() =>
              currentPage === 1
                ? paginate(currentPage)
                : paginate(currentPage - 1)
            }
          >
            {' '}
            {'<'}
          </a>
        </li>
        {pageNumbers.map((number) => (
          <li key={number} className='pagination-list__item '>
            <a
              className={
                currentPage === number
                  ? 'pagination-list__link pagination-list__link--active'
                  : 'pagination-list__link '
              }
              onClick={() => {
                paginate(number);
                addClass();
              }}
              href='#'
            >
              {number}
            </a>
          </li>
        ))}
        <li className='pagination-list__item '>
          <a
            className='pagination-list__link'
            onClick={() =>
              currentPage === pageNumbers.length
                ? paginate(pageNumbers.length)
                : paginate(currentPage + 1)
            }
          >
            {' '}
            {'>'}
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Pagination;
