import { useEffect, useRef, useState } from 'react';
import { BASE_URL } from '../helpers/constants';
export const useApi = (uri) => {
  const [data, setData] = useState([]);
  const [isMounted, setIsMounted] = useState(true);
  const loading = useRef(true);

  useEffect(async () => {
    const result = await fetch(`${BASE_URL}${uri}`)
      .then((res) => res.json())
      .catch((error) => alert(error));

    loading.current = false;
    if (isMounted) {
      setIsMounted(false);
      setData(result);
    }
  }, [uri]);

  return {
    data,
    loading,
  };
};
