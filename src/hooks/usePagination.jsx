import { useState } from 'react';

export const usePagination = (limit) => {
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = limit;
  const indexOfTheLastItem = currentPage * itemsPerPage;
  const indexOfTheFirstItem = indexOfTheLastItem - itemsPerPage;
  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  return {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  };
};
