export const useLocalStorage = () => {
  const getItem = (key) => {
    const item = localStorage.getItem(key);
    return item;
  };

  const setItem = (key, value) => {
    localStorage.setItem(key, value);
    return 'Item saved';
  };

  return {
    getItem,
    setItem,
  };
};
