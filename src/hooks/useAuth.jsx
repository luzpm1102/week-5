import { AUTH } from '../helpers/constants';
import { post } from '../services/post';
import { useLocalStorage } from './useLocalStorage';

export const useAuth = () => {
  const auth = async (identifier, password) => {
    const credentials = {
      identifier: identifier,
      password: password,
    };
    const { setItem } = useLocalStorage();

    try {
      const response = await post(AUTH, credentials);
      const token = await response.jwt;

      if (response.statusCode >= 400) {
        throw new Error(response.error);
      }
      const user = `${response.user.firstName} ${response.user.lastName}`;

      setItem('token', token);
      setItem('user', user);
      alert('Logged In');
    } catch (error) {
      alert('Incorrect password or username');
    }
  };

  return {
    auth,
  };
};
