export const handleChange = (e, set) => {
  set(e.currentTarget.value);
};

export const checkInput = (inputName, value) => {
  if (value === '') {
    alert(inputName + ' required');
    return false;
  }
  return true;
};

export const setInitialState = (set) => {
  set('');
};

export const sort = (a, b) => {
  var dateA = new Date(a);
  var dateB = new Date(b);
  return dateA - dateB;
};
