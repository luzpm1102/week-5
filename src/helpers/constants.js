export const BASE_URL = 'https://trainee-gamerbox.herokuapp.com/';

export const GAMES = 'games/';

export const AUTH = 'auth/local';

export const defaultImage =
  'https://media.istockphoto.com/vectors/no-image-available-icon-vector-id1216251206?k=20&m=1216251206&s=170667a&w=0&h=A72dFkHkDdSfmT6iWl6eMN9t_JZmqGeMoAycP-LMAw4=';
