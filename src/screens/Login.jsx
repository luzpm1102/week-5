import React, { useEffect, useRef, useState } from 'react';
import { Layout } from '../components/Layout';
import { checkInput, handleChange, setInitialState } from '../helpers/helpers';
import { useAuth } from '../hooks/useAuth';
import { useLocalStorage } from '../hooks/useLocalStorage';
import '../styles/login.scss';

export const Login = ({ setNavigation }) => {
  const [user, setUser] = useState('');
  const [pass, setPass] = useState('');
  const { getItem, setItem } = useLocalStorage();
  const [token, setToken] = useState(getItem('token'));
  const userCompleteName = getItem('user');
  const { auth } = useAuth();

  const usernameInput = useRef();
  useEffect(() => {
    if (!token) {
      usernameInput.current.focus();
    }
  }, [token]);

  const authentication = async (user, pass) => {
    if (!checkInput('user', user) || !checkInput('password', pass)) {
      return;
    }
    await auth(user, pass);
    setToken(getItem('token'));
    setInitialState(setUser);
    setInitialState(setPass);
  };

  const handleLogout = () => {
    setItem('token', '');
    setToken('');
    alert('Session Closed');
  };
  return (
    <Layout setNavigation={setNavigation}>
      {!token ? (
        <div className='form-container'>
          <div className='form'>
            <div className='form-body'>
              <label
                htmlFor='username'
                ref={usernameInput}
                className='form-label'
              >
                Username
              </label>
              <input
                type='text'
                id='username'
                className='form-input'
                value={user}
                onChange={(e) => handleChange(e, setUser)}
              />
              <label htmlFor='password' className='form-label'>
                Password
              </label>
              <input
                type='password'
                id='password'
                className='form-input'
                value={pass}
                onChange={(e) => handleChange(e, setPass)}
              />
              <button
                className='form-body__button'
                onClick={() => authentication(user, pass)}
              >
                Login
              </button>
            </div>
          </div>
        </div>
      ) : (
        <div className='user-info'>
          <h3>Welcome {userCompleteName}</h3>
          <button
            onClick={handleLogout}
            className='user-info__button button-primary'
          >
            Log Out
          </button>
        </div>
      )}
    </Layout>
  );
};
