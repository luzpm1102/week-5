import React from 'react';
import { Games } from '../components/Games';
import { Layout } from '../components/Layout';

export const GameList = ({ setNavigation }) => {
  return (
    <Layout setNavigation={setNavigation}>
      <div className='main-container'>
        <div className='header-title-container'>
          <h1 className='header-title'>Games available</h1>
        </div>
        <Games setNavigation={setNavigation} />
      </div>
    </Layout>
  );
};
