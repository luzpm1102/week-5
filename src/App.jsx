import React, { useCallback, useState } from 'react';
import './App.css';
import { GameDetails } from './screens/GameDetails';
import { GameList } from './screens/GameList';
import { HomePage } from './screens/HomePage';
import { Login } from './screens/Login';
import './styles/styles.scss';

export const routes = {
  Home: -1,
  GameList: 0,
  Login: -2,
};

const App = () => {
  const [navigation, setNavigation] = useState(routes.GameList);

  const changeId = useCallback((id) => setNavigation(id), []);

  return (
    <div className='App'>
      {navigation === routes.Home && <HomePage setNavigation={changeId} />}
      {navigation === routes.GameList && <GameList setNavigation={changeId} />}
      {navigation > routes.GameList && (
        <GameDetails id={navigation} setNavigation={changeId} />
      )}
      {navigation === routes.Login && <Login setNavigation={changeId} />}
    </div>
  );
};

export default App;
